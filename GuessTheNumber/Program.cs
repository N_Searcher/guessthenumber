﻿namespace GuessTheNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = new GuessTheNumberGame();
            game.StartGame();
        }
    }
}
