﻿namespace GuessTheNumber
{
    public enum InputResult
    {
        More,
        Less,
        Even,
        OutOfBoundsError,
        NaNError,
        Empty
    }
}