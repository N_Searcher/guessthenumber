﻿using System;

namespace GuessTheNumber
{
    public static class Output
    {
        public static void ShowMessage(string msg, bool nextLine = false)
        {
            Console.Write($"{msg}{(nextLine ? "\n" : "")}");
        }

        public static void ClearOutput()
        {
            Console.Clear();
        }
    }
}
