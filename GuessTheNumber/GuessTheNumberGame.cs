﻿using System;
using MenuLib;
using System.Collections.Generic;

namespace GuessTheNumber
{
    public class GuessTheNumberGame
    {
        private readonly int _lowerBound;
        private readonly int _upperBound;
        private int _wishedValue;
        private readonly Dictionary<InputResult, string> _resultAnswers = new Dictionary<InputResult, string>()
        {
            { InputResult.Less, "lesser" },
            { InputResult.More, "greater" },
            { InputResult.Even, "even" },
            { InputResult.NaNError, "not a number" },
            { InputResult.OutOfBoundsError, "out of bounds" },
        };

        public int WishedValue
        {
            get
            {
                return _wishedValue;
            }
            set
            {
                _wishedValue = value;
            }
        }

        public GuessTheNumberGame(int lBound = 0, int uBound = 100)
        {
            _lowerBound = lBound;
            _upperBound = uBound;
        }

        public void StartGame()
        {
            var menu = new ConsoleMenu(new string[] { "Again", "Exit" });
            string menuItem;

            do
            {
                WishedValue = CreateValue();

                GetInputFromUser();

                Output.ShowMessage("We can play once more!", true);

                menuItem = menu.ShowStartMenu(false);
            } while (menuItem != "Exit");
        }

        public int CreateValue()
        {
            var random = new Random();

            return random.Next(_lowerBound, _upperBound + 1);
        }

        public InputResult AnalyzeValue(string value)
        {
            if (!int.TryParse(value, out int convertedNumber))
                return InputResult.NaNError;
            if (convertedNumber < _lowerBound || convertedNumber > _upperBound)
                return InputResult.OutOfBoundsError;
            if (convertedNumber > WishedValue)
                return InputResult.More;
            if (convertedNumber < WishedValue)
                return InputResult.Less;
            else
                return InputResult.Even;
        }

        private void GetInputFromUser()
        {
            var result = InputResult.Empty;

            while (result != InputResult.Even)
            {
                Output.ShowMessage($"Insert number between {_lowerBound} and {_upperBound} inclusivly:");

                result = AnalyzeValue(Console.ReadLine());

                Output.ShowMessage($"Your value is {_resultAnswers[result]}", true);
            }
        }
    }
}
