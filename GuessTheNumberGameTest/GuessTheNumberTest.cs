using Microsoft.VisualStudio.TestTools.UnitTesting;
using GuessTheNumber;

namespace GuessTheNumberGameTest
{
    [TestClass]
    public class GuessTheNumberTest
    {
        [TestMethod]
        public void WishedValueTest()
        {
            var game = new GuessTheNumberGame(0, 10);

            game.WishedValue = game.CreateValue();

            Assert.IsTrue(game.WishedValue >= 0 && game.WishedValue <= 10);
        }

        [TestMethod]
        public void AnalyzeValueTest()
        {
            var game = new GuessTheNumberGame();

            game.WishedValue = 15;

            Assert.IsTrue(game.AnalyzeValue("10") == InputResult.Less 
                && game.AnalyzeValue("25") == InputResult.More 
                && game.AnalyzeValue("-2") == InputResult.OutOfBoundsError 
                && game.AnalyzeValue("15") == InputResult.Even 
                && game.AnalyzeValue("g") == InputResult.NaNError);
        }
    }
}
